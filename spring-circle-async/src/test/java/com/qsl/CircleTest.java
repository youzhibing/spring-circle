package com.qsl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author: 青石路
 */
@ComponentScan(basePackages = "com.qsl")
@EnableAsync
public class CircleTest {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(CircleTest.class);
        Circle circle = ctx.getBean(Circle.class);
        Loop loop = ctx.getBean(Loop.class);
        System.out.println(circle.getLoop());
        System.out.println(loop);
    }
}
