package com.qsl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @author: 青石路
 */
@Component
public class Loop {

    @Lazy
    @Autowired
    private Circle circle;

    public Circle getCircle() {
        return circle;
    }

    public void sayHello(String name) {
        System.out.println("loop sayHello, " + name);
    }
}
