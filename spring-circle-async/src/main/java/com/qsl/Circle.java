package com.qsl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author: 青石路
 */
@Component
public class Circle {

    @Autowired
    private Loop loop;

    public Loop getLoop() {
        return loop;
    }

    @Async
    public void sayHello(String name) {
        System.out.println("circle sayHello, " + name);
    }
}
