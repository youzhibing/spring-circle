package com.qsl.circle;

/**
 * @author 青石路
 * @date 2021/2/1 20:10
 */
public class Circle {

    private Loop loop;

    public Loop getLoop() {
        return loop;
    }

    public void setLoop(Loop loop) {
        this.loop = loop;
    }

    public void sayHello(String name) {
        System.out.println("hello, " + name);
    }
}
