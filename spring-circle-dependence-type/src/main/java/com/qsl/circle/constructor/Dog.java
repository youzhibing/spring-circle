package com.qsl.circle.constructor;

/**
 * @author 青石路
 * @date 2021/3/10 22:32
 */
public class Dog {

    private Cat cat;

    public Dog(Cat cat) {
        this.cat = cat;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
}
