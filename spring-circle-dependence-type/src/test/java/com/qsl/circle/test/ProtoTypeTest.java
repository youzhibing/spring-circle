package com.qsl.circle.test;

import com.qsl.circle.prototype.Loop;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 青石路
 * @date 2021/2/1 20:17
 */
public class ProtoTypeTest {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("circle-prototype.xml");
        Loop loop = context.getBean("loop", Loop.class);
    }
}
