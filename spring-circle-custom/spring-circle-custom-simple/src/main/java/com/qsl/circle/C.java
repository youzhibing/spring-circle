package com.qsl.circle;

/**
 * @author 青石路
 * @date 2021/2/6 15:54
 */
public class C {

    private Circle circle;

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }
}
