package com.qsl.test;

import com.qsl.circle.C;
import com.qsl.circle.Circle;
import com.qsl.circle.Loop;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 青石路
 * @date 2021/2/1 20:17
 */
public class CircleTest {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("circle-aop.xml");
        Circle circle = (Circle)context.getBean("circle");
        Loop loop = (Loop) context.getBean("loop");
        C c = (C) context.getBean("c");
        System.out.println(circle.getClass().getTypeName());
        System.out.println(loop.getClass().getTypeName());
        System.out.println(c.getClass().getTypeName());
        circle.sayHello("青石路");
    }
}
