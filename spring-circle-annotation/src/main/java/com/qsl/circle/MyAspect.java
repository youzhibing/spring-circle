package com.qsl.circle;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author 青石路
 * @date 2021/2/1 20:13
 */
@Aspect
@Component
public class MyAspect {

    @Pointcut("execution(public void com.qsl.circle.*.sayHello(..))")
    public void beforePointcut() {
    };

    @Before("beforePointcut()")
    public void before() {
        System.out.println("前置增强处理...");
    }
}
