package com.qsl.circle.test;

import com.qsl.circle.Circle;
import com.qsl.circle.Loop;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 青石路
 * @date 2021/2/1 20:10
 */
public class CircleTest {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("circle.xml");
        Circle circle = (Circle)context.getBean("circle");
        Loop loop = (Loop) context.getBean("loop");
        System.out.println(circle.getClass().getTypeName());
        System.out.println(loop.getClass().getTypeName());
        circle.sayHello("青石路");
    }
}
